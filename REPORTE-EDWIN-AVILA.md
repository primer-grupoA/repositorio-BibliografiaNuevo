# REPORTE INVITADO
## COMANDOS
### Nota: Si se puede realizar en los archivos modficaciones, eliminaciones e inserciones, pero solo locamente. Al momento de realizar el PUSH, nos depliega un error.
 * No se puede Agregar, Quitar miembros
 * Puedo editar el archivo de mi compañero pero no puedo realizar PUSH, debido a que la ediciòn es locamente.
 * Si puedo crear una rama, pero no puedo hacer PUSH es decir no puedo visualizar la rama creada en GITLAB
 * NO se puede realizar un PULL:fusionar todos los cambios que se han hecho 
 * Si se puede inicializar un repositorio, pero de igual manera no se puede hacer PUSH
 * Si se puede hacer commit, pero no PUSH
 * Si se puede realizar un Merge entre las ramas que se crearon, pero de igual manera no se puede realizar el PUSH.

## ISSUES
 * No se puede asigar miembros, milestone, labels,weight en los ISSUES.
 * No se puede hacer merge request.
   

# REPORTE REPORTER
## COMANDOS
### Nota: Si se puede realizar en los archivos modficaciones, eliminaciones e inserciones, pero solo locamente. Al momento de realizar el PUSH, nos depliega un error.
## Nota2: El ùnico privilegio que se te tiene es el de hacer el PULL.
 * No se puede Agregar, Quitar miembros
 * Si se puede modificar el archivo de mi compañero, pero no se puede hacer PUSH.
 * Si se puede crear ramas, pero no de igual manera no se puede hacer PUSH.
 * Si se puede hacer PULL, es decir, mi compañero puede hacer cambios y yo puedo descargarme sus cambios.
 * Si se puede hacer commit, pero no PUSH
 * Si se puede inicializar un repositorio, pero de igual manera no se puede hacer PUSH
 * Si se puede realizar un Merge entre las ramas que se crearon, pero de igual manera no se puede realizar el PUSH.

## ISSUES
 * Si puede hacer asignacion de miembros 
 * No se puede hacer merge request.

# REPORTE DEVELOPER
## COMANDOS 
### Nota: Si se puede hacer PUSH  y PULL, pero debemos de tomar en cuenta que existe ciertos privilegios.
 * No se puede Agregar, Quitar miembros
 * Se puede crear una rama, y se puede hacer PUSH.
 * Se puede crear archivos dentro de esa rama y se puede realizar el PUSH.
 * Se puede modificar, insertar y eliminar archivos dentro de la rama.
 * Se puede hacer MERGE entre dos ramas, que se creo.
 * Se puede hacer marge ha la rama MASTER.
 * No se puede estar en la rama master y crear un archivo y luego ahcer PUSH.

## ISSUES
* Si puede hacer asignacion de miembros
* si se puede hacer merge request.
# REPORTE MAINTAINER
## COMANDOS 
### Se puede tener todos los mismos privilegios que tiene el creador del repositorio.
  * Agregar, Quitar miembros
  * Modficaciones, eliminaciones e inserciones de archivos 
* Se puede hacer un merge entre ramas creadas, y de igual manera con el master.
 ## ISSUES 
 * Si puede hacer asignacion de miembros
 * SI se puede hacer merge request.