# Biografía Personal 📋

Edwin Avila C. 🎓

*Cae todas las veces que necesites. Pero nunca dejes de levantarte.*

---
![Foto de Perfil](recursos-edwin-avila/imagenes-edwin-avila/foto-de-perfil.jpg)
<br>

[Foto en facebook](https://scontent.fuio2-1.fna.fbcdn.net/v/t1.0-9/18698066_1543599399080421_1852075143815381119_n.jpg?_nc_cat=0&oh=59512392a87dd54a097e666f884d06f4&oe=5BFCC0C9)


 ***Mi nombre es Edwin Fabricio Avila Cueva, nací en Quito-Ecuador, el 1 de Agosto de 1995. Me crié en un pueblo muy pequeño llamado "Sacapalca" perteneciente a la Provincia de Loja  debido a que mi madre es nacida en ese lugar. 
Mis padres son Angela Graciela Cueva y Washington Benigno Avila, que realizan labores comerciales.
Estudié en la Escuela y Colegio "Abdón Calderón", graduándome en el Área de Informatica así culmine mis estudios anhelando ser un buen profesional ya que siempre me llamo mucho la atención la tecnología.
Con la ayuda incondicional de mis padres e logrado estar donde estoy estudiando en la prestigiosa Escuela Politécnica Nacional, la mejor universidad del país, ahora estoy cursando el septimo semestre de Ingeniería en Sistemas Informaticos y de Computación.
Me esforzaré aprendiendo todo lo que me enseñan los profesores para desempeñarme muy bien en mi vida profesional.***

## Actividades: 🏊
* [Fútbol](recursos-edwin-avila/imagenes-edwin-avila/futbol-edwin.jpg)
* [Ecua-voley](recursos-edwin-avila/imagenes-edwin-avila/ecuavoley-edwin.jpg)
* [Natación](recursos-edwin-avila/imagenes-edwin-avila/natacion-edwin.jpg)
* [Atletismo](recursos-edwin-avila/imagenes-edwin-avila/atletismo-edwin.jpg)

## Mis gustos Musicales:🎶
* Me gusta todo tipo de Música:
 * [Rock en Ingles](https://www.youtube.com/watch?v=1w7OgIMMRc4) 
 * [Rock en Español](https://www.youtube.com/watch?v=xvVLWSsKjkI)
 * [Baladas Romanticas](https://www.youtube.com/watch?v=2wAw_GpJH9U&list=RD2wAw_GpJH9U&start_radio=1)
 * [Reggaeton Antiguo](https://www.youtube.com/watch?v=L4SVtMRdseM)
 * [Pasillos](https://www.youtube.com/watch?v=XD3_wlgVTJ8)

## Como me considero:🏆
Me considero una persona inquieta y creativa, me causa ansiedad no saber ni entender cosas y trato de siempre llenar esos vacíos, me gusta imaginar, soñar e aprendido en este corto trayecto en la universidad, es que las verdades y formas de hacer las cosas, de interpretarlas y de concebirlas son muchas por lo el Estudio es lo mas maravilloso que puede existir.


